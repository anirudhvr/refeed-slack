import os
title = "Devs and Hackers"
description = "Reading list curated by a group of devs"
main_url = "linksbydevs.in"
db_path = os.environ["OPENSHIFT_DATA_DIR"]+"/database.db"
