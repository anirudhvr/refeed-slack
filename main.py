from flask import Flask, Response
from flask import render_template, request, session, redirect, url_for
import rss
import datetime
import dbservice
import math
import settings

app = Flask(__name__)
app.debug = True
app.secret_key = "something very secret should be set in production"
dbservice.bcrypt.init_app(app)


@app.route("/")
def index():
    ITEM_LIMIT = 20
    total_count = dbservice.get_feed_count()
    current_page = request.args.get('page')
    numberOfPages = math.ceil(total_count / ITEM_LIMIT)
    if current_page is None:
        current_page = 1
    else:
        current_page = int(current_page)
    if current_page > numberOfPages:
        current_page = numberOfPages
    limit = (current_page - 1) * ITEM_LIMIT
    list = dbservice.get_item_after(ITEM_LIMIT, limit)
    prev_page = None
    if current_page >= 1:
        prev_page = current_page - 1
    if current_page == numberOfPages:
        current_page = None
    else:
        current_page = current_page + 1
    return render_template("index.html", title=settings.title, count=total_count, feeds=list, prev_page=prev_page, new_page=current_page)


@app.route("/feed")
def get_feed():
    feed = rss.get_rss()
    return Response(feed, mimetype='text/xml')


def is_valid_title(title):
    if title is None:
        return False
    elif len(title) == 0:
        return False
    elif str(title).isspace():
        return False
    else:
        return True


@app.route("/add", methods=['GET', 'POST'])
def add():
    error = None
    if 'login' not in session:
        return redirect(url_for('login'))
    user = dbservice.get_user(username=session['login'])
    if user is None:
        return redirect(url_for('login'))

    if request.method == 'POST':
        url = request.form['url']
        description = request.form['description']
        title = request.form['title']
        existing_item = dbservice.get_feed_by_url(url)
        if existing_item is None:
            print("No existing feed item found")
            item = None
            if is_valid_title(title):
                item = rss.create_feed_item(
                    url, description, user.username, title)
            else:
                item = rss.get_feed_item(url, description, user.username)

            dbservice.store_item(item)
            error = "Success"
        elif rss.is_feed_allowed(existing_item.date, datetime.datetime.now()):
            item = None
            if is_valid_title(title):
                item = rss.create_feed_item(
                    url, description, user.username, title)
            else:
                item = rss.get_feed_item(url, description, user.username)

            dbservice.store_item(item)
            error = "Success"

        else:
            error = "Feed was added less than 7 days ago"
    return render_template("add_item.html", error=error, title=settings.title)


@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'GET':
        if 'login' in session:
            return redirect(url_for('add'))

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = dbservice.authenticate(username, password)
        if user is None:
            error = "User not exists"
        elif user:
            session['login'] = user.username
            return redirect(url_for('add'))
        else:
            error = "Could not authenticate"

    return render_template("login.html", error=error, title=settings.title)


@app.route('/logout')
def logout():
    if 'login' not in session:
        return redirect(url_for('login'))

    session.pop('login', None)
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run()
